# coding=utf-8

import collections
import typing
import csv
import difflib
import hashlib
import inspect
import itertools
import operator
import time
import unittest
import yaml
from copy import deepcopy

__all__ = [
    'Cartman',
    'Case',
    'DictPath',
    'RuleMatcher',
    'RuleOp',
    'Driver',
    'HintingDriver',
    'dataMatch',
    'Plan',
    'yaml_diff',
    'yaml_dump',
]


class Bailout(ValueError):
    pass


class CaseError(ValueError):
    pass


class _Nil:

    def __init__(self):
        if 'NIL' in globals():
            raise ValueError('there can be only one NIL!')

    def __str__(self):
        return 'hoover.NIL'

    def __repr__(self):
        return 'hoover.NIL'

    @staticmethod
    def represent_in_yaml(dumper, value):
        return dumper.represent_none("!NIL")


NIL = _Nil()


# ########################################################################### #
# ## The Pattern                                                           ## #
# ########################################################################### #

class RuleOp:

    def __init__(self, items, item_ok):
        self._items = items
        self._item_ok = item_ok

    def _eval(self, item):
        try:                                    # it's a pattern! (recurse)
            return RuleMatcher.Match(item, self._item_ok)
        except ValueError:                      # no, it's something else...
            return self._item_ok(item)

    def __bool__(self):
        try:
            return self._match()
        except TypeError:
            raise ValueError("items must be an iterable: %r" % self._items)


class RuleMatcher:

    class ALL(RuleOp):

        def _match(self):
            return all(self._eval(item) for item in self._items)

    class ANY(RuleOp):

        def _match(self):
            return any(self._eval(item) for item in self._items)

    @staticmethod
    def Match(pattern, item_ok):
        """
        Evaluate set of logically structured patterns using passed function.

        *pattern* must be a tuple in form of `(op, items)` where *op* can be
        either `RuleMatcher.ALL` or `RuleMatcher.ANY` and *items* is a list of
        items to check using *item_ok* function.

        *item_ok* is a function that accepts single argument and its return
        value is evaluated for true-ness.

        Final result is True or False and is computed by combining results
        of individual *item_ok* calls: either all must be true (when `op
        == RuleMatcher.ALL`) or at least one must be true (when
        `op == RuleMatcher.ANY`).

        The evaluation is done recursively, that is, if an item in the pattern
        is also a pattern itself, it will be evaluated by calling
        RuleMatcher.Match and passing the same *item_ok* function.

        Note that there is no data to evaluate "against", you can use closure
        if you need to do that.
        """

        try:
            op, items = pattern
        except TypeError:
            raise ValueError("pattern is not a tuple: %r" % pattern)
        if type(op) is not type:
            raise ValueError("invalid operator: %r" % op)
        if not issubclass(op, RuleOp):
            raise ValueError("invalid operator class: %s" % op.__name__)
        return bool(op(items, item_ok))


# ########################################################################### #
# ## The Path                                                              ## #
# ########################################################################### #

class DictPath:
    """Mixin that adds "path-like" behavior to the top dict of dicts.

    Use this class as a mixin for a deep dictionary-like structure in order to
    access the elements using a Unix-like path.  For example:

        MyData(dict, DictPath):
            pass

        d = MyData({
            'name': 'Joe',
            'age': 34,
            'ssn': {
                'number': '012 345 678',
                'expires': '10-01-16',
            },
        })

        print ("%s's ssn number %s will expire on %s"
                % (d.getpath('/name'),
                   d.getpath('/ssn/number'),
                   d.getpath('/ssn/expiry')))
        # joe's ssn number 012 345 678 will expire 10-01-16
    """

    DIV = "/"

    class Path:

        def __init__(self, path, div):
            self.DIV = div
            self._path = path

        def _validate(self):
            try:
                has_root = self._path.startswith(self.DIV)
            except AttributeError:
                raise ValueError("invalid path: not a string: %r" % self._path)
            if not has_root:
                raise ValueError("invalid path: missing root: %r" % self._path)

        def stripped(self):
            return self._path.lstrip(self.DIV)

    @classmethod
    def __s2path(cls, path):
        return cls.Path(path, cls.DIV)

    @classmethod
    def __err_path_not_found(cls, path):
        raise KeyError("path not found: %s" % path)

    @classmethod
    def __getitem(cls, dct, key):
        if cls.DIV in key:
            frag, rest = key.split(cls.DIV, 1)
            subdct = dct[frag]
            result = cls.__getitem(subdct, rest)
        else:
            result = dct[key]
        return result

    @classmethod
    def __setitem(cls, dct, key, value):
        if cls.DIV not in key:
            dct[key] = value
        else:
            frag, rest = key.split(cls.DIV, 1)
            subdct = dct[frag]
            cls.__setitem(subdct, rest, value)

    @classmethod
    def __delitem(cls, dct, key):
        if cls.DIV not in key:
            del dct[key]
        else:
            frag, rest = key.split(cls.DIV, 1)
            subdct = dct[frag]
            return cls.__delitem(subdct, rest)

    # # public methods
    #

    def getpath(self, path):
        try:
            return self.__getitem(self, self.__s2path(path).stripped())
        except (TypeError, KeyError):
            self.__err_path_not_found(path)

    def setpath(self, path, value):
        try:
            self.__setitem(self, self.__s2path(path).stripped(), value)
        except (TypeError, KeyError):
            self.__err_path_not_found(path)

    def delpath(self, path):
        try:
            self.__delitem(self, self.__s2path(path).stripped())
        except (TypeError, KeyError):
            self.__err_path_not_found(path)

    def ispath(self, path):
        try:
            self.getpath(path)
            return True
        except KeyError:
            return False


# ########################################################################### #
# ## The Case                                                              ## #
# ########################################################################### #

class CallReport(dict, DictPath):
    """
    Single oracle/result driver call report.

    After obtaining data from calling oracle driver and test driver, this
    class is used to perform set of hacks, do actual comparison and store all
    the data for later tracking.

    Instantiate CallReport with a dictionary with following items:

     *  'case' - case data as fed into `Driver.run`,
     *  'hacks' - list of hack rulesets as described below,
     *  'match_op' - callable to be used for comparison,
     *  'result' and 'result_raw' - data returned from result driver,
     *  'oracle' and 'oracle_raw' - data returned from oracle driver,
     *  'result_driver' - # name of result driver's class,
     *  'oracle_driver' - # name of oracle driver's class.

    During initialization 'hacks' are applied and 'match_op' is called with
    'result' and 'oracle' keys as its argument.  The boolean return value of
    'match_op' determines whether the test has passed (True) or failed (False).

    The initialized CallReport will contain few more keys and attributes:

     *  'hacks_applied' key - list of individual hack rules that matched and
        were applied; see below for details.

     *  'diff' and 'diff_id' attributes - the 'diff' is a string holding
        unified diff of pretty-formatted 'result' and 'oracle' key.  'diff_id'
        is formed by making SHA1 of the 'diff' and taking first 7 characters
        of hexadecimal representation; this is similar to short hash in Git.

        If the test was considered pass (by 'match_op'), then both 'diff' and
        'diff_id' are empty strings.

     *  'key' attribute - tuple that can be used as a key when storing
        CallReport in a dictionary, or for sorting.  The tuple will consist
        of 'diff_id', 'oracle_driver' and 'result_driver'.

     *  'data' attribute - a convenience dictionary describing the details of
        the test result; in particular: 'case', 'oracle' and 'result' after
        applying hacks, 'oracle_raw', 'result_raw' and 'hacks_applied'.


    Hacks
    -----

    ### Disclaimer ###

    Generally, hacks go counter to best development and testing practice.
    When used incorrectly, a hack can even contribute to cement bugs in
    System Under Test!

    That said, the mechanism exists in order to help deal with very specific
    types of known issues and inconsistencies, largely legacy code bugs.

    Hacks should be only used as a last resort:

     *  If your oracle driver is getting really complex just to deal with
        a very specific case, hack could be a lesser evil.

     *  If you are considering skipping the affected test cases, hacks could
        save them.

    In either case, hacks should used only after careful risk assessment and
    in a transparent manner.  For example, tracking and listing hacks as part
    of quality metric can be advisable.


    ### Usage ###

    Hacks are sets of rules in in a following form:

        {
            'drivers': [{}],        # list of structures to match
                                    # against self
            'cases': [{}],          # -ditto-
            <action_name>: <Arg>    # an action name with argument
            <action_name>: <Arg>    # another action...
        }

    Each of the rules is first evaluated for match (does it apply to this
    CallReport?), and if the rule applies, transformation is done.  The
    transformation is defined by `<action_name>: <Arg>` pairs and it can
    alter 'oracle', 'result' or both.

    The match evaluation is done using `hoover.dataMatch()` -- this is
    basically a recursive pattern match against 'drivers' and 'cases'.  Both
    'drivers' and 'cases' are optional, but when specified, all items must must
    match in order for the rule to apply.  (If 'drivers' and 'cases' are both
    missing or empty, rule will apply to each and all test cases.)

    If rule does not match, next one is evaluated.

    If a rule does match, each action is applied.  Action consists of action
    name (key of the rule dictionary, <action_name>) and an argument (<Arg>).

    Action name must be one of: 'remove', 'even_up', 'format_str', 'exchange'
    or 'round'.  Each action corresponds to a CallReport method prefixed by
    'a_'; for example 'even_up' action corresponds to CallReport.a_even_up
    method.  Each action expects different argument so see the corresponding
    method docstrings.

    Because 'oracle' and 'result' can be relatively complex structures, actions
    accept Unix-like paths to specify elements inside them.  The "root" of the
    path is the CallReport instance, and "directories" are keys under it.

    For example, following would be valid paths if test drivers work with
    dictionaries such as `{'temperature': 50, 'stats': {'word_count': 15}}`:

        /oracle/temperature
        /result/stats/word_count

    Warning: All actions will silently ignore any paths that are invalid
             or leading to non-existent data!
             (This does not apply to a path leading to `None`.)
    """

    def __init__(self, value):
        super().__init__(value)
        self['oracle'] = deepcopy(self['oracle'])
        self._apply_hacks()
        self.diff_id, self.diff = self._mkdiff()
        self.key = (
            self.diff_id,
            self['oracle_driver'],
            self['result_driver'],
        )
        self.data = {
            'case': self['case'],
            'hacks_applied': self['hacks_applied'],
            'oracle': self['oracle'],
            'oracle_driver': self['oracle_driver'],
            'oracle_raw': self['oracle_raw'],
            'result': self['result'],
            'result_driver': self['result_driver'],
            'result_raw': self['result_raw'],
        }

    def _apply_hacks(self):
        matched_rules = []
        for h in self['hacks']:
            matched_rules += self._hack(h)
        self['hacks_applied'] = matched_rules

    def _mkdiff(self):
        """
        Create diff of result vs. oracle; return its id and string

        If self['match_op'] says that self['result'] and self['oracle']
        match, then return tuple of two empty strings.

        Otherwise, pretty-format both self['result'] and self['oracle'],
        create a unified diff, and return tuple containing diff ID and the
        diff string itself.

        The diff ID is formed by making SHA1 hash of the diff string and taking
        first 7 characters of hexadecimal representation---similar to short
        hash in Git.
        """
        if self['match_op'](self['result'], self['oracle']):
            return '', ''
        diff = yaml_diff(
            dira=self['result'],
            dirb=self['oracle'],
            namea=self['result_driver'],
            nameb=self['oracle_driver'],
            chara='r',
            charb='o',
        )
        return hashlib.sha1(diff.encode()).hexdigest()[:7], diff

    def a_exchange(self, action):
        """Exchange value A for value B.

        Expects a dict, where key is a tuple of two values `(a, b)` and
        value is a list of paths.  For each key, it goes through the
        paths and if the value equals `a` it is set to `b`.
        """
        for (oldv, newv), paths in action.items():
            for path in paths:
                try:
                    curv = self.getpath(path)
                except KeyError:
                    continue
                else:
                    if curv == oldv:
                        self.setpath(path, newv)

    def a_format_str(self, action):
        """Convert value to a string using format string.

        Expects a dict, where key is a format string, and value is a list
        of paths.  For each record, the paths are traversed, and value is
        converted to string using the format string and the `%` operator.

        This is especially useful for floats which you may want to trim
        before comparison, since direct comparison of floats is unreliable
        on some architectures.
        """
        for fmt, paths in action.items():
            for path in paths:
                if self.ispath(path):
                    new = fmt % self.getpath(path)
                    self.setpath(path, new)

    def a_even_up(self, action):
        """Even up structure of both dictionaries.

        Expects a list of two-element tuples `('/dict/a', '/dict/b')`
        containing pairs of path do simple dictionaries.

        Then the two dicts are altered to have same structure: if a key
        in dict "a" is missing in dict "b", it is set to `None` in "b" and
        vice-versa,
        """
        for patha, pathb in action:
            try:
                a = self.getpath(patha)
                b = self.getpath(pathb)
            except KeyError:
                continue
            else:
                for key in set(a.keys()) | set(b.keys()):
                    if key in a and key in b:
                        pass    # nothing to do here
                    elif key in a and a[key] is None:
                        b[key] = None
                    elif key in b and b[key] is None:
                        a[key] = None
                    else:
                        pass    # bailout: odd key but value is *not* None

    def a_remove(self, action):
        """Remove elements from structure.

        Expects a simple list of paths that are simply deleted from the
        structure.
        """
        for path in action:
            if self.ispath(path):
                self.delpath(path)

    def a_round(self, action):
        """Round a (presumably) float using `float()` built-in.

        Expects dict with precision (ndigits, after the dot) as a key and
        list of paths as value.
        """
        for ndigits, paths in action.items():
            for path in paths:
                try:
                    f = self.getpath(path)
                except KeyError:
                    pass
                else:
                    self.setpath(path, round(f, ndigits))

    known_actions = {'remove': a_remove,
                     'even_up': a_even_up,
                     'format_str': a_format_str,
                     'exchange': a_exchange,
                     'round': a_round}

    def _hack(self, ruleset):
        """
        Run any matching actions in the *ruleset*.

        """

        def driver_matches(rule):
            if 'drivers' not in rule:
                return True
            else:
                return any(dataMatch(p, self)
                           for p in rule['drivers'])

        def case_matches(rule):
            if 'cases' not in rule:
                return True
            else:
                return any(dataMatch(p, self)
                           for p in rule['cases'])

        matched_rules = [
            rule for rule in ruleset
            if driver_matches(rule) and case_matches(rule)
        ]
        cls = self.__class__
        for rule in matched_rules:
            for action_name in cls.known_actions:
                if action_name in rule:
                    cls.known_actions[action_name](self, rule[action_name])
        return matched_rules


# ########################################################################### #
# ## Drivers                                                               ## #
# ########################################################################### #

class DriverError(Exception):
    """Error encountered when obtaining driver data"""

    def __init__(self, message, driver, args):
        self.message = message
        self.driver = driver
        self.case_args = args   # Built-in Exception already uses `args`

    def __str__(self):

        result = ("\n\n"
                  "  type: %s\n"
                  "  message: %s\n"
                  "  driver: %s\n"
                  "  args: %s\n"
                  "  settings: %s\n"
                  % (self.message.__class__.__name__,
                     self.message,
                     self.driver.__class__.__name__,
                     self.case_args,
                     self.driver.settings))

        return result


class DriverDataError(Exception):
    """Error encountered when decoding or normalizing driver data"""

    def __init__(self, exception, driver, args, raw_data):
        self.exception = exception
        self.driver = driver
        self.case_args = args   # Built-in Exception already uses `args`
        self.raw_data = raw_data

    def __str__(self):

        result = ("%s: %s\n"
                  "  class: %s\n"
                  "  args: %s\n"
                  "  raw_data: %s\n"
                  % (self.exception.__class__.__name__, self.exception,
                     self.driver.__class__.__name__,
                     yaml_dump(self.case_args),
                     yaml_dump(self.raw_data)))
        return result


def _check_xor(dct, a, b, ecls):
    """
    Verify that dictionary *dct* has either *a* or *b*, but not both keys.

    If one of these keys are present, do nothing.

            CaseError, "missing one of mandatory arguments",
    If all are missing, raise exception from class *ecls* with message
    *msg* and list of alternatives.
    """
    if a in dct and b not in dct:
        return
    if b in dct and a not in dct:
        return
    if a in dct and b in dct:
        raise ecls("cannot provide both: %s and %s" % (a, b))
    raise ecls("must provide one of: %s or %s" % (a, b))


def _check_casedict(case):
    """
    Verify that *case* is a valid case dictionary.

    If it is, do nothing.

    Otherwise, raise CaseError with identified problem.
    """
    generic = "case must be dict with dict under 'args' key: "
    if not isinstance(case, dict):
        raise CaseError(generic + f"case is {type(case).__name__}")
    if 'args' not in case:
        raise CaseError(generic + f"no 'args' in {set(case.keys())}")
    a = case['args']
    if not isinstance(a, dict):
        raise CaseError(generic + f"case['args'] is {type(a).__name__}")
    return


def _check_missing(dct, needed, ecls, msg):
    """
    Verify that dictionary *dct* has all needed keys in *needed* list.

    If all keys are present, do nothing.

    If some are missing, raise exception from class *ecls* with message
    *msg* and list of missing keys.
    """
    missing = set(needed) - set(dct.keys())
    if not missing:
        return
    raise ecls("%s: %r" % (msg, missing))


def _flt_dotted(dct, root, dotless_pass=True):
    """
    Filter out dotted keys from *dct* that belong to *root*

    Each key in *dct* dictionary must have one of forms:

        ROOT.LEAF
        LEAF

    This function returns a dictionary that contains LEAF keys that
    were prefixed by ROOT provided as *root* argument or by asterisk
    (`*`).  If ROOT is not provided, keys are always passed.

    If *dotless_pass* is False the second form is prohibited, i.e.
    all keys must contain the ROOT part.
    """
    out = {}
    for k, v in dct.items():
        if dotless_pass and ('.' not in k):
            out[k] = v
            continue
        kroot, kleaf = k.split('.', maxsplit=1)
        if kroot in [root, '*']:
            out[kleaf] = v
    return out


class Driver:
    """
    Base class for test drivers used by *Plan*.

    This class tepresents test driver and can be used to:

     *  Wrap system under test (SUT).

        Provide simple interface to set up, sandbox and activate the system
        and collect any relevant results.  This can be merely return value
        (purely functional test) but also other characteristics such as
        time to complete.

     *  Mimic ("mock") the system under test.

        Also called as oracle machine, this can be used to predict expected
        behavior of SUT under given parameters.

     *  Wrap an alternative implementation of SUT.

        As a special case of the previous role, sometimes it's desirable to
        use an alternative implementation of SUT as oracle machine.  This
        can be a legacy implementation, reference implementation or other
        platform implementation.

    In either case, the driver makes sure that any input arguments are
    interpreted (and passed on) correctly and any results are returned in
    a consistent way.

    Sub-classes must implement *fetch()* method, which must accept keyword
    arguments found in 'args' key of test case, that is, each item in
    *cases* as passed to *Plan()* or *Case.run_plan()*.  *fetch()*
    method must return result from the SUT.  See *Driver.fetch()*
    for details.

    Optionally, you can implement also following:

     *  *mandatory_args*, list of keys that must be present in case data.
        If a key is missing, *CaseError* is raised.

     *  *mandatory_settings*, list of keys that must be present in driver
        settings.  If a key is missing, *DriverError* is raised.

        Driver settings are generated by *Plan* and are shared by all
        drivers; see *Plan.driver_settings* for details.

     *  *bailouts*, list of functions that must accept single argument.
        Before running the driver, each of these functions will be called
        with 'args' key of the *case*, and True return value from one
        or more will result in the test case being skipped.

     *  methods *cook_rv()* and *cook_ex()* used to decode, normalize and
        validate the data before comparison.  See *Driver.run()* for details.

     *  methods *prune_rv()* and *prune_ex()* used similarly to *cook_rv()*
        and *cook_ex()*, except that their return value is not used by the
        test but kept to for later investigation.  See *Driver.run()* for
        details.
    """

    bailouts: list[typing.Callable[[list], None]] = []
    mandatory_args: list = []
    mandatory_settings: list = []
    valid_exceptions: list = []

    def __init__(self, settings=None, sut_factory=None):
        """
        Initialize driver with *settings* and *sut_factory*.
        """
        self.duration = 0
        self.settings = settings
        self.sut_factory = sut_factory

    def _check_case(self, case):
        _check_casedict(case)
        _check_missing(
            case['args'], self.mandatory_args,
            CaseError, "missing mandatory SUT arguments",
        )

    def _load_sut_factory(self, case):
        if self.sut_factory is None:
            return case.get('sut_factory')
        if 'sut_factory' in case:
            raise CaseError(
                "sut_factory already set globally, cannot combine both"
            )
        return self.sut_factory

    def _try_fetch(self, case):
        def is_valid_ex(ex):
            names = [i for i in self.valid_exceptions
                     if isinstance(i, str)]
            types = tuple([i for i in self.valid_exceptions
                          if isinstance(i, type)])
            ecls = ex.__class__
            en = ecls.__name__
            fqen = ecls.__module__ + '.' + ecls.__name__
            return fqen in names or en in names or isinstance(ex, types)
        try:
            start = time.time()
            raw_rv = self.fetch(**case['args'])
            self.duration = time.time() - start
            return raw_rv, NIL
        except Exception as e:
            if is_valid_ex(e):
                return NIL, e
            raise DriverError(e, self, case)

    @classmethod
    def check_values(cls, args):
        """
        Check args in advance before running or setting up anything.
        """
        for fn in cls.bailouts:
            if fn(args):
                raise NotImplementedError(inspect.getsource(fn))

    def fetch(self, **args):
        pass

    def cook_rv(self, raw_rv, args):
        return raw_rv

    def cook_ex(self, raw_ex, args):
        return raw_ex

    def prune_rv(self, raw_rv, args):
        return raw_rv

    def prune_ex(self, raw_ex, args):
        return raw_ex

    def run(self, case):
        """
        Validate *case*, run SUT with it and store data.

        *case* must be a dictionary with following keys:

         *  'args' - arguments for SUT (System Under Test).

         *  'sut_factory' (optional) - callable that accepts no arguments and
             produces reference to SUT that can be used when obtaining result
             value.  This is only allowed if *sut_factory* was not provided via
            *Plan* initializer.

        There are five methods that can affect the result of the driver run:
        *fetch()*, *cook_rv()*, *cook_ex()*, *prune_rv()* and *prune_ex()*.
        Only *fetch()* needs to be implemented by sub-class, the other methods
        have default no-op implementations that just return the argument.

        Before running any of the methods, *case* is checked 'sut_factory'.
        key.  If this key is found, its value is assigned to attribute of the
        same name.  Note that if *sut_factory* argument was passed to *Plan*
        initializer, it will be already assigned to the attribute and
        overriding it with the case key will lead to *CaseError()* being
        raised.  This is to discourage from error-prone strategies of defining
        test cases; either all test cases share the same way of initializing
        SUT or they define it explicitly.

        First method that is called is *fetch()*.  The *fetch()* method is
        called with keyword arguments found in 'args' key of the *case*.  Time
        it takes for *fetch()* to return is measured, and this value is stored
        in 'duration' attribute.

        Further processing depends on following possible scenarios:

         *  (A) *fetch()* has returned a value,
         *  (B) *fetch()* has raised a valid exception,
         *  (C) *fetch()* has raised an invalid exception.

        Valid exceptions are exceptions whose classes, ancestor classes or
        class names are listed in *valid_exceptions* attribute.

        If invalid exception was raised (C), it's re-raised as
        *DriverError()* with additional details of which case excactly
        raised it in which driver instance.

        If there was no exception (A), the return value is passed to
        *cook_rv()* method and *prune_rv()* method.  The first result
        is later used for test comparison by *Plan*.  The second result is
        only stored for later investigation.

        The scenario (B) is almost identical, except the other pair of methods
        is used: *cook_ex()* and *prune_ex()*.

        The purpose of *cook_rv()* and *cook_ex()* is to decode,
        normalize, validate and make basic inferrences from the return
        value or exception, respectively.  As mentioned above, either
        of these values is later used in the comparison by *Plan*.

        Purpose of *prune_rv()* and *prune_ex()* methods is to decide whether,
        or which parts of, the raw data should be preserved and displayed in
        case of failure.  Return values from these methods do not affect
        comparison done by *Plan* (i. e., they don't affect result of the
        test), so they can contain non-deterministic or extraneous details
        which could be useful when a test actually does fail.

        Finally, a tuple of three values, two dictionaries and one number,
        is returned: first dictionary holds the cooked data, second one holds
        the pruned data.  Both of these dictionaries have single key, which
        is 'rv' if there was no exception, and 'ex' if there was exception.
        Final item in the tuple is the duration it took for *fetch()* to
        return.
        """

        def try_finalize(raw, fn):
            try:
                return fn(raw, case['args'])
            except Exception as e:
                raise DriverDataError(e, self, case, raw)
        self._check_case(case)
        self.sut_factory = self._load_sut_factory(case)
        raw_rv, raw_ex = self._try_fetch(case)
        if raw_ex == NIL:
            return (
                {'rv': try_finalize(raw_rv, self.cook_rv)},
                {'rv': try_finalize(raw_rv, self.prune_rv)},
                self.duration,
            )
        else:   # valid exception was raised
            return (
                {'ex': try_finalize(raw_ex, self.cook_ex)},
                {'ex': try_finalize(raw_ex, self.prune_ex)},
                self.duration,
            )


class HintingDriver(Driver):
    """
    Similar to Driver, except that none of *fetch()*, *cook_\\*()*
    or *prune_\\**()* methods are used.  Instead, the equivalent data is read
    from the *case* itself.

    Where this driver is used, *case* must provide either one, but not both of
    following keys:

     *  'hint' - predicted return value of the SUT,
     *  'hint_ex' - predicted "cooked" exception from the SUT.

    Note that setting 'hint_ex' means that SUT is expected to raise
    exception -- in such case it's meaningless to set also 'hint'.
    This goes both ways, it's meaningless to set 'hint_ex' if a return
    value is expected.  Therefore, if both keys are seen, CaseError
    is raised.

    These keys are directly used to form the return of *run()* method,
    whereas the raw data is always empty dictionary and the duration is
    always zero.
    """

    def _check_case(self, case):
        _check_casedict(case)
        _check_xor(
            case, 'hint', 'hint_ex',
            CaseError,
        )
        _check_missing(
            case['args'], self.mandatory_args,
            CaseError, "missing mandatory SUT arguments",
        )

    def run(self, case):
        self._check_case(case)
        self.sut_factory = self._load_sut_factory(case)
        if 'hint_ex' in case:
            data = {'ex': deepcopy(case['hint_ex'])}
        elif 'hint' in case:
            data = {'rv': deepcopy(case['hint'])}
        else:
            raise RuntimeError("BUG: this should not be!")
        return (
            data,
            {},
            self.duration,
        )


class TrueMockDriver(Driver):
    """A simple mock driver, always returning True"""

    def fetch(self, **kwargs):
        return True


# ########################################################################### #
# ## Helpers                                                               ## #
# ########################################################################### #

class _StatCounter:
    """
    A simple counter with support for custom formulas.
    """

    def __init__(self):
        self.generic_stats = {}
        self.driver_stats = {}
        self.formulas = {}
        self._born = time.time()

    def _register(self, dname):
        self.driver_stats[dname] = {
            'calls': 0,
            'rhacks': 0,
            'ohacks': 0,
            'duration': 0,
            'overhead': 0
        }

        ##
        # Formulas
        #

        # cumulative duration/overhead; just round to ms
        self.add_formula(dname + '_overhead',
                         lambda g, d: int(1000 * d[dname]['overhead']))
        self.add_formula(dname + '_duration',
                         lambda g, d: int(1000 * d[dname]['duration']))

        # average (per driver call) overhead/duration
        self.add_formula(
            dname + '_overhead_per_call',
            lambda g, d: int(1000 * d[dname]['overhead'] / d[dname]['calls'])
        )
        self.add_formula(
            dname + '_duration_per_call',
            lambda g, d: int(1000 * d[dname]['duration'] / d[dname]['calls'])
        )

        def gtotal_drivertime(g, d):
            driver_time = (sum(s['overhead'] for s in d.values())
                           + sum(s['duration'] for s in d.values()))
            return int(1000 * driver_time)

        def gtotal_loop_overhead(g, d):
            driver_time = gtotal_drivertime(g, d)
            onnext_time = int(1000 * g['on_next'])
            age = int(1000 * (time.time() - self._born))
            return age - driver_time - onnext_time

        # grand totals in times: driver time, loop overhead
        self.add_formula('gtotal_drivertime', gtotal_drivertime)
        self.add_formula('gtotal_loop_overhead', gtotal_loop_overhead)
        self.add_formula('gtotal_loop_onnext',
                         lambda g, d: int(1000 * g['on_next']))

        # average (per driver call) overhead/duration
        self.add_formula(
            'calls_hacked',
            lambda g, d: round(100 * float(g['hacked_calls']) / g['calls'], 2)
        )

    def _computed_stats(self):
        computed = dict.fromkeys(self.formulas)
        for fname, fml in self.formulas.items():
            try:
                v = fml(self.generic_stats, self.driver_stats)
            except ZeroDivisionError:
                v = None
            computed[fname] = v
        return computed

    def add_formula(self, vname, formula):
        """
        Add a function to work with generic_stats, driver_stats.
        """
        self.formulas[vname] = formula

    def add(self, vname, value):
        """
        Add a value to generic stat counter.
        """
        if vname in self.generic_stats:
            self.generic_stats[vname] += value
        else:
            self.generic_stats[vname] = value

    def add_for(self, dclass, vname, value):
        """
        Add a value to driver stat counter.
        """
        dname = dclass.__name__
        if dname not in self.driver_stats:
            self._register(dname)
        if vname in self.driver_stats[dname]:
            self.driver_stats[dname][vname] += value
        else:
            self.driver_stats[dname][vname] = value

    def count(self, vname):
        """
        Alias to add(vname, 1)
        """
        self.add(vname, 1)

    def count_for(self, dclass, vname):
        """
        Alias to add_for(dclass, vname, 1)
        """
        self.add_for(dclass, vname, 1)

    def all_stats(self):
        """
        Compute stats from formulas and add them to colledted data.
        """
        stats = self.generic_stats
        for dname, dstats in self.driver_stats.items():
            for key, value in dstats.items():
                stats[dname + "_" + key] = value
        stats.update(self._computed_stats())
        return stats


class PlanReport(dict):
    """
    Fail tracker to allow for usable reports from huge regression tests.

    Best used as a result bearer from `PlanReport`, this class keeps
    a simple in-memory "database" of fails seen during the regression
    test, and implements few methods to access the data.

    The basic usage is:

         1. Instantiate (no parameters)

         2. Each time you have a result of a test, you pass it to `update()`
            method along with the argument set (as a single object, typically
            a dict) that caused the fail.

            If boolean value of the result is False, the object is thrown away
            and nothing happens.  Otherwise, its string value is used as a key
            under which the argument set is saved.

            The string interpretation of the result is supposed to be
            "as deterministic as possible", i.e. it should provide only
            necessary information about the fail:  do not include any
            timestamps or "volatile" values such as PID's, version numbers
            or tempfile names.

         3. At final stage, you can retrieve statistics as how many (distinct)
            fails have been recorded, what was the duration of the whole test,
            how many times `update()` was called, etc.

         4. Optionally, you can also call `format_report()` to get a nicely
            formatted report with list of arguments for each fail string.

         5. Since in bigger tests, argument lists can grow really large,
            complete lists are not normally printed.  Instead, you can use
            `write_stats_csv()`, which will create one CSV per each fail,
            named as first 7 chars of its SHA1 (inspired by Git).

            Note that you need to pass an existing writable folder path.
    """

    ##
    #  internal methods
    #

    def __init__(self):
        self._start = time.time()
        self._reports = {}
        self._reports_by_diff = {}
        self.tests_done = 0
        self.tests_passed = 0
        self.cases_done = 0
        self.driver_stats = {}

    def _csv_fname(self, call_report, prefix):
        """
        Format name of file for this call_report
        """
        return '%s/%s.csv' % (prefix, call_report.diff_id)

    def _insert(self, call_report):
        """
        Insert the call_report into DB.
        """
        self._reports[call_report.key] = call_report.data
        if call_report.diff not in self._reports_by_diff:
            self._reports_by_diff[call_report.diff] = []
        self._reports_by_diff[call_report.diff].append(call_report)

    def _format_fail(self, call_report, max_ca=0):
        """
        Format single fail for output.
        """
        return '\n'.join(self._format_fail_lines(call_report, max_ca))

    def _format_fail_lines(self, diff, max_ca=0):
        """
        Format single fail for output.
        """
        cases_togo = self._reports_by_diff[diff].copy()
        diff = cases_togo[0].diff
        diff_id = cases_togo[0].diff_id
        num_ttl = len(cases_togo)
        yield ""
        yield "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        yield f"~~~ FAIL ({diff_id}) ~~~~~~~~~~~~~~~~~~~~~~~~~"
        yield "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        yield "--- raw result example: ---------------------------"
        for yl in yaml_dump(cases_togo[0]['result_raw']).split('\n'):
            yield ' |' + yl
        yield "--- result/oracle diff ------------------------------"
        yield diff
        yield f"--- cases affected ({num_ttl}) ---------------------------"
        credit = max_ca
        while cases_togo:
            if max_ca and not credit:
                yield (f"[...] not showing {len(cases_togo)} cases,"
                       f" see {diff_id}.csv for full list")
                break
            yield str(cases_togo.pop(0)['case'])
            credit -= 1

    ##
    #  public methods
    #

    def fails_found(self):
        """
        True if any fails were seen.
        """
        return bool(self._reports)

    def format_report(self, max_ca=0):
        """
        Return complete report formatted as string.
        """
        fail_list = "\n".join([self._format_fail(e, max_ca=max_ca)
                               for e in self._reports_by_diff])
        return ("Found %(total_fails)s (%(distinct_fails)s distinct) fails"
                " in %(tests_done)s tests with %(cases)s cases"
                " (duration: %(time)ss):"
                % self.getstats()
                + "\n\n" + fail_list)

    def getstats(self):
        """
        Return basic and driver stats

        Returns dictionary with following keys:

         *  'tests_done' - how many times PlanReport.update() was called

         *  'distinct_fails' - how many distinct fails (same call_report.diff)

         *  'total_fails' - how many times any fail was seen

         *  'time'       - how long since init (seconds)
        """

        stats = {
            "cases": self.cases_done,
            "tests_done": self.tests_done,
            "distinct_fails": len(self._reports_by_diff),
            "total_fails": len(self._reports),
            "time": int(time.time() - self._start)
        }
        stats.update(self.driver_stats)
        return stats

    def update(self, call_report):
        """
        Update tracker with test result.

        If call_report.diff is true, it is the call_report is considered
        fail and kept in internal dictionary under call_report.key as key.
        This allows for later sorting and analysis.
        """
        self.tests_done += 1
        if call_report.diff:
            self._insert(call_report)

    def write_stats_csv(self, fname):
        """
        Write stats to a simple one row (plus header) CSV.
        """
        stats = self.getstats()
        colnames = sorted(stats.keys())
        with open(fname, 'a') as fh:
            cw = csv.DictWriter(fh, colnames)
            cw.writerow(dict(zip(colnames, colnames)))  # header
            cw.writerow(stats)

    def write_args_csv(self, prefix=''):
        """
        Write out a set of CSV files, one per distinctive fail.

        Each CSV is named with diff ID (first 7 chars of SHA1) and lists
        all argument sets affected by this fail.  This is supposed to make
        easier to further analyse impact and trigerring values of fails,
        perhaps using a table processor software.
        """

        def get_all_colnames():
            cn = {}
            for affected in self._reports_by_diff.values():
                for call_report in affected:
                    cn.update(dict.fromkeys(call_report['case']))
                return sorted(cn.keys())

        all_colnames = get_all_colnames()

        for diff in self._reports_by_diff:
            with open(self._csv_fname(diff, prefix), 'a') as fh:
                cw = csv.DictWriter(fh, all_colnames)
                cw.writerow(dict(zip(all_colnames, all_colnames)))  # header
                for call_report in self._reports_by_diff[diff]:
                    cw.writerow(call_report['case'])

    def write_yaml(self, fname):
        data = {v[0].diff_id: {'diff': k, 'cases': [c.data for c in v]}
                for k, v in self._reports_by_diff.items()}
        with open(fname, 'w') as fh:
            return yaml_dump(data, stream=fh)


def dataMatch(pattern, data):
    """
    Check if data structure matches a pattern data structure.

    Supports lists, dictionaries and scalars (int, float, string).

    For scalars, simple `==` is used.

    Lists are converted to sets and "to match" means "to have a matching
    subset (e.g. `[1, 2, 3, 4]` matches `[3, 2]`).

    Both lists and dictionaries are matched recursively.
    """

    def are_dictlikes(*objs):
        return all([isinstance(o, dict) for o in objs])

    def are_listlikes(*objs):
        return all([isinstance(o, list) for o in objs])

    def listMatch(pattern, data):
        """
        Match list-like objects
        """
        results = []
        for pv in pattern:
            if any([dataMatch(pv, dv) for dv in data]):
                results.append(True)
            else:
                results.append(False)
        return all(results)

    def dictMatch(pattern, data):
        """
        Match dict-like objects
        """
        results = []
        try:
            for pk, pv in pattern.items():
                results.append(dataMatch(pv, data[pk]))
        except KeyError:
            results.append(False)
        return all(results)

    if are_dictlikes(pattern, data):
        return dictMatch(pattern, data)
    if are_listlikes(pattern, data):
        return listMatch(pattern, data)
    return pattern == data


def yaml_dump(data, stream=None):
    """
    A human-readable YAML dump.
    """
    return yaml.dump(data, stream=stream, sort_keys=True, indent=4)


def yaml_diff(dira, dirb, namea="A", nameb="B", chara="a", charb="b"):
    dumpa = yaml_dump(dira)
    dumpb = yaml_dump(dirb)
    dumpa_dlines = ['|' + line for line in dumpa.split('\n')]
    dumpb_dlines = ['|' + line for line in dumpb.split('\n')]
    udiff = difflib.unified_diff(dumpa_dlines, dumpb_dlines,
                                 "~/" + namea, "~/" + nameb,
                                 n=10000, lineterm='')

    def rechar(line):
        if line.startswith('---'):
            return chara + chara + chara + line[3:]
        if line.startswith('+++'):
            return charb + charb + charb + line[3:]
        if line.startswith('-'):
            return chara + line[1:]
        if line.startswith('+'):
            return charb + line[1:]
        return line

    return "\n".join([rechar(line) for line in udiff])


class Cartman:
    """
    Create argument sets from ranges (or ay iterators) of values.

    This class is to enable easy definition and generation of dictionary
    argument sets using Cartesian product.

    To use Cartman iterator, you need to define structure of an argument
    set.  Argument set--typically a dictionary--is a set of values that
    together constitute a test case.  Within the argument set, values
    will change from test case to test case, so for each changing value,
    you will also need to define range of values you want to test on.

    Cartman initiator expects following arguments:

     *  *scheme*, which is a "prototype" of a final argument set, except
        that values are replaced by either `Cartman.Iterable` if the
        value is changing from test case to another, and `Cartman.Scalar`
        if the value is constant.

     *  *source*, which has the same structure, except that where in scheme
        is `Cartman.Iterable`, the source has an iterable.  Where scheme has
        `Cartman.Scalar`, the source can have any value.

    Finally, when Cartman instance is used in loop, it uses Cartesian product
    in order to generate argument sets.

    Consider this example:

        You have a system (wrapped up in test driver) that takes ''size''
        argument, that is supposed to be ''width'', ''height'' and ''depth'',
        each an integer ranging from 1 to 100, and ''color'' that can
        be "white", "black" or "yellow".

        For a test using all-combinations strategy, you will need to generate
        100 * 100 * 100 * 3 argument sets, i.e. 3M tests.

        All you need to do is:

            scheme = {
                'size': {
                    'width': Cartman.Iterable,
                    'height': Cartman.Iterable,
                    'depth': Cartman.Iterable,
                }
                'color': Cartman.Iterable,
            }

            source = {
                'size': {
                    'width': range(1, 100),
                    'height': range(1, 100),
                    'depth': range(1, 100),
                }
                'color': ['white', 'black', 'yellow'],
            }

            c = Cartman(source, scheme)

            for case in c:
                result = my_test(case)
                # assert ...

    The main advantage is that you can separate the definition from
    the code, and you can keep yor iterators as big or as small as
    needed, and add / remove values.

    Also in case your parameters vary in structure over time, or from
    one test to another, it gets much easier to keep up with changes
    without much jumping through hoops.

    Note: `Cartman.Scalar` is provided mainly to make your definitions
    more readable.  Following constructions are functionally equal:

        c = Cartman({'a': 1}, {'a': Cartman.Scalar})
        c = Cartman({'a': [1]}, {'a': Cartman.Iterable})

    In future, however, this might change, though, mainly in case
    optimization became possible based on what was used.
    """

    # TODO: support for arbitrary ordering (profile / nginx)
    # TODO: implement getstats and fmtstats
    # TODO: N-wise

    class _Mark:
        pass

    class Scalar(_Mark):
        pass

    class Iterable(_Mark):
        pass

    def __init__(self, source, scheme):
        self.source = source
        self.scheme = scheme

        # validate scheme + source and throw useful error
        scheme_ok = isinstance(self.scheme, collections.abc.Mapping)
        source_ok = isinstance(self.source, collections.abc.Mapping)
        if not scheme_ok:
            raise ValueError("scheme must be a mapping (e.g. dict)")
        elif scheme_ok and not source_ok:
            raise ValueError("scheme vs. source mismatch")

    def __deepcopy__(self, memo):
        return Cartman(deepcopy(self.source, memo),
                       deepcopy(self.scheme, memo))

    def _is_mark(self, subscheme):
        try:
            return issubclass(subscheme, Cartman._Mark)
        except TypeError:
            return False

    def _means_scalar(self, subscheme):
        if self._is_mark(subscheme):
            return issubclass(subscheme, Cartman.Scalar)

    def _means_iterable(self, subscheme):
        if self._is_mark(subscheme):
            return issubclass(subscheme, Cartman.Iterable)

    def _get_iterable_for(self, key):
        subscheme = self.scheme[key]
        subsource = self.source[key]
        if self._means_scalar(subscheme):
            return [subsource]
        elif self._means_iterable(subscheme):
            return subsource
        else:   # try to use it as scheme
            return iter(Cartman(subsource, subscheme))

    def __iter__(self):

        names = []
        iterables = []

        for key in self.scheme:
            try:
                iterables.append(self._get_iterable_for(key))
            except KeyError:
                pass    # ignore that subsource mentioned by scheme is missing
            else:
                names.append(key)

        for values in itertools.product(*iterables):
            yield dict(zip(names, values))

    def getstats(self):
        return {}

    def fmtstats(self):
        return ""


class DriverFactory:

    def __init__(self, cls, settings, sut_factory):
        self.cls = cls
        local_settings = _flt_dotted(settings, cls.__name__)
        _check_missing(
            local_settings, cls.mandatory_settings,
            DriverError, "missing driver settings",
        )
        self.kwargs = {
            'settings': local_settings,
            'sut_factory': sut_factory,
        }

    def make(self):
        return self.cls(**self.kwargs)


class Plan:

    def __init__(self, cases, driver_classes,
                 driver_settings=None, sut_factory=None,
                 match_op=None, hacks=None):
        """
        *driver_classes* must be list of two or more classes that are derived
        from *Driver* master class.  First class in the list represents oracle
        driver and remaining classes represent result classes.

        For each case in *cases*, each driver is instantiated and called to
        produce result.  The details of the driver call are described in
        *Driver.run()* method.

        Unless a driver has raised an error, return values from these driver
        calls are compared such that each value obtained from a result driver
        is compared to the value obtained from the oracle driver.

        For example, if *driver_classes* contains FooDrv, BarDrv and BazDrv,
        then each case is evaluated three times, and two comparisons are made.
        The value from FooDrv is compared to value from BarDrv, then the same
        value is compared to value from BazDrv.

        If a driver raises error, whole process is aborted, except for specific
        case: when a driver raises NotImplementedError from *check_values()*
        method, then just single case is skipped.

        Keys in *driver_settings* must be in one of following forms:

            KEY             # with no periods
            CLASSNAME.KEY
            *.KEY

        In the first form, they key and its value is simply passed to the
        driver.  In the second form, key and its value are ignored unless
        CLASSNAME matches driver class name; in that case the prefix is
        removed and the value is passed with the key part.  The third form
        is same as the second form except that it will match any driver.

        Note that the third form is useful used if the key is universal to
        all drivers but also contains embedded period.

        The value of *sut_factory* will be assigned to instance attribute of
        the same name.  Typically, *sut_factory* will a callable that helps
        initialize System Under Test.

        If *hacks* is provided, it must be a list of rulesets as described
        in *CallReport* class.  Just before value comparison described above,
        hacks are selectively applied to the compared data and can alter it.
        Unlike standard driver methods *fetch()*, *cook()*, *cook_rv()* and
        *cook_ex()*, a hack rule can be conditioned on specific values found
        in case arguments, oracle data or result data, or combination of these.

        **Warning:** Hacks go counter to best development and testing practice.
        When used incorrectly, a hack can even contribute to fasten misbehaving
        System Under Test!  See *CallReport* for closer discussion on when and
        how to use hacks safely.
        """
        self.cases = cases
        self.__check_drivers(driver_classes)
        self.driver_factories = [
            DriverFactory(dc, driver_settings, sut_factory)
            for dc in driver_classes
        ]
        self.match_op = match_op or operator.eq
        self.hacks = hacks or []
        self._counter = None
        self._last_case = None
        self._report = None

    def __count_hacks(self, call_report, ofac, rfac):
        done = len(call_report['hacks_applied'])
        self._counter.add_for(ofac.cls, 'ohacks', done)
        self._counter.add_for(rfac.cls, 'rhacks', done)
        self._counter.add('hacks', done)
        self._counter.add('hacked_calls', (1 if done else 0))

    def __check_drivers(self, classes):
        if len(classes) < 2:
            raise ValueError(
                "too few driver classes; need one oracle driver"
                " and at least one result driver: %s"
                % ([cls.__name__ for cls in classes])
            )

    def __on_next(self, this_case):
        on_start = time.time()
        self.on_next(this_case, self._last_case)
        self._counter.add('on_next', time.time() - on_start)
        if self._last_case is None:
            self._last_case = this_case

    def __get_data(self, case, driver_fac):
        """
        Obtain test case result data
        """
        start = time.time()
        driver = driver_fac.make()
        data, raw_data, duration = driver.run(case)
        overhead = time.time() - duration - start
        self._counter.count('calls')
        self._counter.count_for(driver_fac.cls, 'calls')
        self._counter.add_for(driver_fac.cls, 'duration', duration)
        self._counter.add_for(driver_fac.cls, 'overhead', overhead)
        return data, raw_data

    def __check_bailouts(self, case):
        for fac in self.driver_factories:
            try:
                fac.cls.check_values(case['args'])
            except NotImplementedError as e:         # bailout!
                self._counter.count_for(fac.cls, 'bailouts')
                raise Bailout(e)

    def __run_case(self, case):
        self.__on_next(case)
        self.__check_bailouts(case)
        ofac = self.driver_factories[0]
        oracle, oracle_raw = self.__get_data(case, ofac)
        for rfac in self.driver_factories[1:]:
            result, result_raw = self.__get_data(case, rfac)
            call_report = CallReport({
                'case': case,
                'match_op': self.match_op,
                'hacks': self.hacks,
                'oracle': oracle,
                'result': result,
                'oracle_raw': oracle_raw,
                'result_raw': result_raw,
                'oracle_driver': ofac.cls.__name__,
                'result_driver': rfac.cls.__name__
            })
            self.__count_hacks(call_report, ofac, rfac)
            self._report.update(call_report)
        self._report.cases_done += 1
        self._counter.count('cases')

    def on_next(self, this_case, prev_case):
        pass

    def run(self):
        self._report = PlanReport()
        self._counter = _StatCounter()
        for case in self.cases:
            try:
                self.__run_case(case)
            except Bailout:
                continue
        self._report.driver_stats = self._counter.all_stats()
        return self._report


class Case(unittest.TestCase):

    driver_settings: dict[str, typing.Any] = {}
    hacks = None
    match_op = None
    driver_classes: list[Driver] = []
    yaml_file = None
    args = NIL
    driver = None
    hint = NIL
    hint_ex = NIL
    sut_factory = None

    def run_plan(self, cases, driver_classes=None):
        """
        Run test plan formed from *cases*.

        Prior to calling this method, you must set *driver_classes*
        attribute to a list of at least two classes (not instances!)
        which are sub-class of *hoover.Driver* class.  First of the
        drivers will be considered oracle (prediction) driver, the
        second driver and any further driver will be considered result
        (measurement) driver.  Generally, only two drivers are used.

        Result drivers must implement at least *fetch()* method, with
        the exception described below.  See *Driver* and *HintingDriver*
        for details on how to implement powerful drivers with minimum
        effort.

        *cases* must be a list of dictionaries, where each dictionary
        describes a test case, and must contain at least 'args' key.
        Content of this key will be passed to *fetch()* method of all
        drivers specified in *driver_classes*, described below.
        Custom drivers might require additional keys.

        Oracle driver (the first driver in driver_classes list) can be
        sub-classed from *HintingDriver*, which removes the need for
        implementing *fetch()*, but requires that one (but not both!)
        of two additional keys must be present in each case dictionary:
        'hint' or 'hint_ex', which correspond to expected return value
        and, if exception is predicted, expected exception.

        Optionally, you can set *yaml_file* attribute to a file path.
        If set, the file will be populated with YAML report containing
        list of failing test cases along with some details.

        You can also set several optional attributes, each of which is
        described in detail in *hoover.Plan* docstring.  Here's just
        a short summary of each:

         *  *driver_settings*, a dictionary which will be used to populate
            *settings* attribute of the driver during its initialization.

         *  *sut_factory*, a callable that will be set to *sut_factory*
            attribute of the driver.

         *  *match_op*, a two-argument callable that will be used to compare
            return values from result driver and oracle driver.

         *  *hacks*, a list of rulesets that will be used to alter the data
            before the comparison.
        """
        driver_classes = driver_classes or self.driver_classes
        plan = Plan(
            cases=cases,
            driver_classes=driver_classes,
            driver_settings=self.driver_settings,
            sut_factory=self.sut_factory,
            match_op=self.match_op,
            hacks=self.hacks,
        )
        report = plan.run()
        if self.yaml_file:
            report.write_yaml(self.yaml_file)
        if report.fails_found():
            self.fail(report.format_report())

    def run_self(self):
        """
        Run a micro test plan formed from values assigned to *self*.

        Prior to calling this method, you must set following attributes
        to *self*:

         *  *driver* - a sub-class (not instance) of *hoover.Driver*,
            implementing at least *fetch()* method.

         *  *args* - a dictionary of arguments to be passed to driver's
            *fetch()* method.

         *  either *hint* - a value that will be compared to ("cooked",
            if driver implements *cook_rv()*) value returned by *fetch()*,

         *  or *hint_ex* - a value that will be compared to ("cooked", if
            driver implements *cook_ex()*) exception raised from *fetch()*.

        For *args*, *hint* and *hint_ex*, the standard Python None can be
        a valid value, so hoover does not consider None as unset value.
        Instead, *hoover.NIL* is used for this purpose and these attributes
        are re-set to *hoover.NIL* with every call of *run_self()*.

        Optionally, you can set most of the attributes described in
        *run_plan()*, except for the *driver_classes*, which is here
        replaced here by *driver* attribute.
        """
        def mv(aname):
            value = getattr(self, aname)
            if value == NIL:
                return {}
            setattr(self, aname, NIL)
            return {aname: value}
        if self.driver is None:
            raise CaseError("must set 'driver' attribute")
        if self.args == NIL:
            raise CaseError("must set 'args' attribute")
        if self.hint == NIL and self.hint_ex == NIL:
            raise CaseError("must set 'hint' or 'hint_ex' attribute")
        self.run_plan(
            cases=[mv('args') | mv('hint') | mv('hint_ex')],
            driver_classes=[HintingDriver, self.driver],
        )
