#!/usr/bin/python3

import requests
import subprocess
import unittest

import hoover


def bailout_on_zerodiv(case):
    if case['op'] == 'div':
        return case['b'] == 0
    return False


class CalcDriver(hoover.Driver):

    mandatory_args = ['op', 'a', 'b']
    bailouts = [bailout_on_zerodiv]


class PyCalcDriver(CalcDriver):

    def fetch(self, a, b, op):
        ops = {
            'add': lambda a, b: float(a) + b,
            'sub': lambda a, b: float(a) - b,
            'mul': lambda a, b: float(a) * b,
            'div': lambda a, b: float(a) / b,
        }
        return ops[op](a, b)


class CgiCalcDriver(CalcDriver):

    mandatory_settings = ['uri']

    def fetch(self, a, b, op):
        params = {'op': op, 'a': a, 'b': b}
        response = requests.get(
            url=self.settings['uri'],
            params=params,
        )
        assert response.status_code == 200
        return float(response.text)


class CliCalcDriver(CalcDriver):

    mandatory_settings = ['cmd']

    def fetch(self, a, b, op):
        cmd = [
            self.settings['cmd'],
            op,
            str(a),
            str(b),
        ]
        out = subprocess.check_output(cmd)
        return float(out.strip())


class Case(hoover.Case):

    driver_classes = [PyCalcDriver, CliCalcDriver, CgiCalcDriver]
    driver_settings = {
        'uri': "http://127.0.0.1:8887/cgi-bin/calc.cgi",
        'cmd': "./calc.sh"
    }

    def test_using_rt(self):
        scheme = {'args': dict.fromkeys(
            ['op', 'a', 'b'],
            hoover.Cartman.Iterable,
        )}
        cases = hoover.Cartman({'args': {
            'op': ['add', 'sub', 'mul', 'div'],
            'a': [-10, -1, 0, 1, 10, 1000],
            'b': [-10, -1, 0, 1, 10, 1000],
            }}, scheme
        )
        self.run_plan(cases)


if __name__ == '__main__':
    unittest.main()
