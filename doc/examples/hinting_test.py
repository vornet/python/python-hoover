#!/usr/bin/python3

import operator
import unittest

import hoover


class MyOracleDriver(hoover.HintingDriver):
    pass


class MyResultDriver(hoover.Driver):

    mandatory_args = ['a', 'b']

    def fetch(self, a, b):          # this will be fed from each cases's *args*
        return self.sut_factory()(a, b)
        #                           # this will be compared to 'oracle'


class OpTest:

    driver_classes = [MyOracleDriver, MyResultDriver]
    yaml_file = 'hoover.yaml'

    def test_all_cases(self):
        # remember that hoover.Case does
        # .. not give us any `test_*` methods
        self.run_plan(self.cases)


#
# Double inheritance is not nice, but..
#
# Cleaner way is making OpTest from hoover.Case (which is
# a unittest.TestCase) and inheriting just from OpTest.
#
# However, that makes unittest load and run it.
#
# Other way to prevent would be to mess with the loading
# mechanism but that canm be error prone.
#
# The best way, however, becomes easy once your test arsenal
# grows it makes sense to move the OpTest (and perhaps drivers
# as well) to separate module, say, util.py, and load it here:
# that prevents unittest from loading it and also makes for
# much cleaner test code base.
#

class MyPowTest(OpTest, hoover.Case):

    cases = [
        {'args': {'a': 1, 'b': 0}, 'hint': 1},
        {'args': {'a': 1, 'b': 1}, 'hint': 1},
        {'args': {'a': 1, 'b': 2}, 'hint': 1},
        {'args': {'a': 2, 'b': 0}, 'hint': 1},
        {'args': {'a': 2, 'b': 1}, 'hint': 2},
        {'args': {'a': 2, 'b': 2}, 'hint': 4},
        {'args': {'a': 2, 'b': 3}, 'hint': 8},
    ]

    def sut_factory(self):
        return operator.pow


class MyAddTest(OpTest, hoover.Case):

    cases = [
        {'args': {'a': 1, 'b': 0}, 'hint': 1},
        {'args': {'a': 1, 'b': 1}, 'hint': 2},
        {'args': {'a': 1, 'b': 2}, 'hint': 3},
        {'args': {'a': 2, 'b': 0}, 'hint': 2},
        {'args': {'a': 2, 'b': 1}, 'hint': 3},
        {'args': {'a': 2, 'b': 2}, 'hint': 4},
        {'args': {'a': 2, 'b': 3}, 'hint': 5},
    ]

    def sut_factory(self):
        return operator.add


if __name__ == '__main__':
    unittest.main()
